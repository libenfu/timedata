#多种方案
nginx 日志方式统计
nginx lua redis 记录并发统计, 需要下面的nginx 配置  

## nginx 编译配置
        [root@bf nginx]#nginx -V
        nginx version: nginx/1.10.3
        built by gcc 6.1.0 (GCC)
        built with OpenSSL 1.0.1e-fips 11 Feb 2013
        TLS SNI support enabled
        configure arguments: --prefix=/usr/local/nginx --user=www --group=www --with-http_stub_status_module --with-http_ssl_module --with-pcre=/usr/src/make/software/pcre-8.38 --add-module=/usr/src/make/software/redis2-nginx-module-master --add-module=../ngx_devel_kit-0.3.0 --add-module=../lua-nginx-module-0.10.8


## nginx.conf

        user  www;
        worker_processes  1;

        error_log  /data/logs/nginx/error.log;
        #pid logs/nginx.pid;
        events {
            use epoll;
            worker_connections  1024;
        }

        http {
            include mime.types;
            default_type application/octet-stream;
            #日志格式
            log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                             '$status $body_bytes_sent "$http_referer" '
                             '"$http_user_agent" "$http_x_forwarded_for"';

            lua_package_path "/usr/local/nginx/lua-resty-redis-0.26/lib/?.lua;;";

            #access_log  /data/logs/nginx/access.log  main;
            #关闭错误页面的nginx 版本号 这样安全性是有好处的
            #server_tokens off;
            sendfile on;
            #tcp_nopush on;

            #keepalive_timeout 0;
            keepalive_timeout 65;

            #开启gzip
            gzip  on;
            gzip_min_length 1k;
            gzip_buffers 16 64k;
            gzip_http_version 1.1;
            gzip_comp_level 6;
            gzip_types text/plain application/x-javascript text/css application/xml;
            gzip_vary on;

            #解决跨域
            add_header Access-Control-Allow-Origin *;
            add_header Access-Control-Allow-Headers X-Requested-With;
            add_header Access-Control-Allow-Methods GET,POST,OPTIONS;

            include conf.d/*.conf;

            #autoindex on;
            #autoindex_exact_size off;
            #autoindex_localtime on;
            index index.php index.html index.htm;
            access_log /data/logs/nginx/$server_name.log  main;

            #error_page 404 /html/404.html;
            #error_page 500 502 503 504 /html/50x.html;
        }


## conf.d/test.conf

        server {
            listen 80;
            server_name test.com;
            root /data/www;

            access_log /data/logs/$server_name.access.log  main;

            if ($http_user_agent ~ "DNSPod") {
                return 200;
            }

            try_files $uri $uri/ @rewrite;

            location @rewrite {
                rewrite ^/(.*)$ /index.php?_url=/$1;
            }

            location /status {
                stub_status on;
                auth_basic "NginxStatus";
                access_log  off;
            }

            location ~ \.php {

                fastcgi_pass unix:/dev/shm/php-fpm.sock;
                fastcgi_index index.php;

                fastcgi_split_path_info ^(.+\.php)(.*)$;
                fastcgi_param PATH_INFO $fastcgi_path_info;

                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;

                default_type text/html;
                lua_code_cache on;
                rewrite_by_lua_file /usr/local/nginx/lua/test_redis_basic.lua;
            }
        }
