-- 分割字符串
function split(str, split_char)
	local sub_str_tab = {};
	while (true) do
		local pos = string.find(str, split_char);
		if (not pos) then
			sub_str_tab[#sub_str_tab + 1] = str;
			break;
		end
		local sub_str = string.sub(str, 1, pos - 1);
		sub_str_tab[#sub_str_tab + 1] = sub_str;
		str = string.sub(str, pos + 1, #str);
	end

	return sub_str_tab;
end

-- 接受Nginx传递进来的参数$1 也就是SteamName
local request_uri = ngx.var.request_uri
local uri_arr = split(request_uri, '/')
local uri = string.format("%s:%s", uri_arr[2], uri_arr[3])

local redis = require("resty.redis");
-- 创建一个redis对象实例。在失败，返回nil和描述错误的字符串的情况下
local redis_instance = redis:new();

-- 设置后续操作的超时（以毫秒为单位）保护，包括connect方法
redis_instance:set_timeout(1000)

-- 建立连接
local ip = '127.0.0.1'
local port = 6397

-- 尝试连接到redis服务器正在侦听的远程主机和端口
local ok,err = redis_instance:connect(ip, port)
if not ok then
	ngx.say("connect redis error : ", err)
	return err
end

-- 选择数据库
redis_instance:select(0)

-- URI请求数量时间到秒
local reshincr, err = redis_instance:hincrby('request:second:'..uri, os.time(), 1)
if not reshincr then
	ngx.say("request:second error: ", err)
	return err
end

